import React from 'react';
import { Typography, Container, Grid, Card, CardContent, List, ListItem, ListItemText } from '@mui/material';
import SquadsData from "./SquadsData";

function ListTemplate() {
    const squads = SquadsData();

    return (
        <Container maxWidth="lg" sx={{ py: 4 }}>
            <Typography variant="h4" align="center" gutterBottom>
                List of Squads
            </Typography>
            <Grid container spacing={3} justifyContent="center">
                {squads.map((squad) => (
                    <Grid key={squad.id} item xs={12} sm={6} md={4}>
                        <Card variant="outlined">
                            <CardContent>
                                <Typography variant="h6" gutterBottom>
                                    {squad.squadName}
                                </Typography>
                                <List disablePadding>
                                    {squad.players.map((player, index) => (
                                        <ListItem key={index} disableGutters>
                                            <ListItemText
                                                primary={player.name}
                                                secondary={`Goals: ${player.goals}, Assists: ${player.assists}`}
                                            />
                                        </ListItem>
                                    ))}
                                </List>
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            <footer style={{ marginTop: '20px', textAlign: 'center' }}>
                <Typography variant="body2">
                    © {new Date().getFullYear()} YourWebsite. All Rights Reserved.
                </Typography>
            </footer>
        </Container>
    );
}

export default ListTemplate;