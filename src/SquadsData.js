// SquadsData.js
import React from 'react';

const SquadsData = () => {
    const squads = [
        {
            id: 1,
            squadName: 'Team A',
            players: [
                { name: 'Player 1', goals: 25, assists: 15 },
                { name: 'Player 2', goals: 20, assists: 10 },
                // Add more players with their statistics as needed
            ],
        },
        {
            id: 2,
            squadName: 'Team B',
            players: [
                { name: 'Player 3', goals: 30, assists: 20 },
                { name: 'Player 4', goals: 22, assists: 12 },
                // Add more players with their statistics as needed
            ],
        },
        // Add more squads as needed
    ];

    return squads;
};

export default SquadsData;